import React from 'react';
import Svg, {Path} from 'react-native-svg';
import { View, ViewProps } from 'react-native';
import Icon from './Icon';

interface CrossedEyeIconProps extends Icon {
    containerStyle?: any
}

const CrossedEyeIcon : React.FC<CrossedEyeIconProps> = (props: CrossedEyeIconProps) => {
    return (
        <View {...props.containerStyle}>
            <Svg width={props.width || 40} height={props.height || 40} viewBox="0 0 18 16" fill="none">
                <Path d="M1 8.11542C1 8.11542 4.2 2.5 9 2.5C13.8 2.5 17 8.11542 17 8.11542C17 8.11542 13.8 13.7308 9 13.7308C4.2 13.7308 1 8.11542 1 8.11542Z" stroke="#828282" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="square"/>
                <Path d="M8.99985 10.5219C10.3254 10.5219 11.3999 9.44436 11.3999 8.11523C11.3999 6.7861 10.3254 5.70862 8.99985 5.70862C7.67437 5.70862 6.59985 6.7861 6.59985 8.11523C6.59985 9.44436 7.67437 10.5219 8.99985 10.5219Z" stroke="#828282" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="square"/>
                <Path d="M16 1L2 15" stroke="#828282" />
            </Svg>
        </View>
    )
}

export default CrossedEyeIcon;
import React from 'react';
import Svg, {Path} from 'react-native-svg';
import { View, ViewProps } from 'react-native';
import Icon from './Icon';

interface BellIconProps extends Icon {
    containerStyle?: any
}

const BellIcon : React.FC<BellIconProps> = (props: BellIconProps) => {
    return (
        <View {...props.containerStyle}>
            <Svg width={props.width || 40} height={props.height || 40} viewBox="0 0 22 22" fill="none">
                <Path d="M13.6662 18.3389C13.6662 19.8117 12.4677 21.0002 10.9949 21.0002C9.52204 21.0002 8.3335 19.8117 8.3335 18.3389" stroke="#BDBDBD" stroke-width="1.5" stroke-linecap="square" strokeWidth="1.5"/>
                <Path d="M1 15.667H21" stroke="#BDBDBD" stroke-width="1.5" stroke-linecap="square" strokeWidth="1.5"/>
                <Path d="M19.6694 15.6673H20.3298C18.857 15.6673 16.9981 14.4788 16.9981 13.006V7.00322C16.9981 3.69189 14.3167 1 10.9952 1C7.68383 1 5.00211 3.69189 5.00211 7.00322V13.006C5.00211 14.4788 3.80364 15.6673 2.33081 15.6673" stroke="#BDBDBD" stroke-width="1.5" stroke-linecap="square" strokeWidth="1.5"/>
            </Svg>
        </View>
    )
}

export default BellIcon;


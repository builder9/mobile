import React from 'react';
import Svg, {Path} from 'react-native-svg';
import { View, ViewProps } from 'react-native';
import Icon from './Icon';

interface WrenchIconProps extends Icon {
    containerStyle?: any
}

const WrenchIcon : React.FC<WrenchIconProps> = (props: WrenchIconProps) => {
    return (
        <View {...props.containerStyle}>
            <Svg width={props.width || 40} height={props.height || 40} viewBox="0 0 23 23" fill="none">
                <Path fill-rule="evenodd" clip-rule="evenodd" d="M21.7139 4.61935L18.8568 7.47641L15.5236 4.14318L18.3806 1.28612C17.8664 1.09565 17.3138 0.991075 16.7615 1.0006C13.8758 1.02917 11.5521 3.35291 11.5235 6.23854C11.5235 6.7909 11.6186 7.33393 11.809 7.85772L2 16.2382L6.76176 21L15.1423 11.2858C15.6565 11.4858 16.2091 11.5813 16.7615 11.5718C19.6471 11.5432 21.9708 9.21949 21.9994 6.33387C22.0089 5.75293 21.9139 5.17172 21.7139 4.61935Z" stroke="#B6B6B6" stroke-width="1.5" stroke-linecap="square" strokeWidth="1.5"/>
            </Svg>
        </View>
    )
}

export default WrenchIcon;

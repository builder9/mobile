import React from 'react';
import Svg, {Path, G, Defs, ClipPath, Rect} from 'react-native-svg';
import { View, ViewProps } from 'react-native';
import Icon from './Icon';

interface ChatIconProps extends Icon {
    containerStyle?: any
}

const ChatIcon : React.FC<ChatIconProps> = (props: ChatIconProps) => {
    return (
        <View {...props.containerStyle}>
            <Svg width={props.width || 40} height={props.height || 40} viewBox="0 0 22 22" fill="none">
                <G clip-path="url(#clip0)">
                    <Path d="M18.125 5.625C18.4565 5.625 18.7745 5.7567 19.0089 5.99112C19.2433 6.22554 19.375 6.54348 19.375 6.875V13.75C19.375 14.0815 19.2433 14.3995 19.0089 14.6339C18.7745 14.8683 18.4565 15 18.125 15H16.875V18.125L12.5 15H8.75" stroke="#BDBDBD" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="square"/>
                    <Path d="M14.375 1.25H1.875C1.54348 1.25 1.22554 1.3817 0.991117 1.61612C0.756696 1.85054 0.625 2.16848 0.625 2.5V10.625C0.625 10.9565 0.756696 11.2745 0.991117 11.5089C1.22554 11.7433 1.54348 11.875 1.875 11.875H3.75V16.25L8.75 11.875H14.375C14.7065 11.875 15.0245 11.7433 15.2589 11.5089C15.4933 11.2745 15.625 10.9565 15.625 10.625V2.5C15.625 2.16848 15.4933 1.85054 15.2589 1.61612C15.0245 1.3817 14.7065 1.25 14.375 1.25Z" stroke="#BDBDBD" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="square"/>
                </G>
                <Defs>
                    <ClipPath id="clip0">
                        <Rect width="20" height="20" fill="white"/>
                    </ClipPath>
                </Defs>
            </Svg>
        </View>
    )
}

export default ChatIcon;
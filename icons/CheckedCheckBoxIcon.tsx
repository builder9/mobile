import React from 'react';
import Svg, {Path, G, Defs, ClipPath, Rect, Circle} from 'react-native-svg';
import { View, ViewProps, ViewStyle } from 'react-native';
import Icon from './Icon';

interface CheckedCheckBoxIconProps extends Icon {
    containerStyle?: ViewStyle
}

const CheckedCheckBoxIcon : React.FC<CheckedCheckBoxIconProps> = (props: CheckedCheckBoxIconProps) => {
    return (
        <View {...props.containerStyle}>
            <Svg width={props.width || 40} height={props.height || 40} viewBox="0 0 20 20" fill="#3686FF">
                <Circle cx="10" cy="10" r="9.5" stroke="#3686FF" />
            </Svg>
        </View>
    )
}

export default CheckedCheckBoxIcon;
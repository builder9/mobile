import React from 'react';
import Svg, {Path} from 'react-native-svg';
import { View, ViewProps, ViewStyle } from 'react-native';
import Icon from './Icon';

interface SearchIconProps extends Icon {
    containerStyle?: ViewStyle
}

const SearchIcon : React.FC<SearchIconProps> = (props: SearchIconProps) => {
    return (
        <View {...props.containerStyle}>
            <Svg width={props.width || 40} height={props.height || 40} viewBox="0 0 18 18" fill="none">
                <Path d="M17 16.9998L13.8 13.7998" stroke="#B6B6B6" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                <Path d="M7.4 13.7999C10.9346 13.7999 13.8 10.9346 13.8 7.39997C13.8 3.86537 10.9346 1 7.4 1C3.86538 1 1 3.86537 1 7.39997C1 10.9346 3.86538 13.7999 7.4 13.7999Z" stroke="#B6B6B6" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
            </Svg>
        </View>
    )
}

export default SearchIcon;

import React from 'react';
import Svg, {Path, G, Defs, ClipPath, Rect, Circle} from 'react-native-svg';
import { View, ViewProps, ViewStyle } from 'react-native';
import Icon from './Icon';

interface UncheckedCheckBoxIconProps extends Icon {
    containerStyle?: ViewStyle
}

const UncheckedCheckBoxIcon : React.FC<UncheckedCheckBoxIconProps> = (props: UncheckedCheckBoxIconProps) => {
    return (
        <View {...props.containerStyle}>
            <Svg width={props.width || 40} height={props.height || 40} viewBox="0 0 20 20" fill="none">
                <Circle cx="10" cy="10" r="9.5" stroke="#B6B6B6" />
            </Svg>
        </View>
    )
}

export default UncheckedCheckBoxIcon;
import React from 'react';
import Svg, {Path} from 'react-native-svg';
import { View, ViewProps } from 'react-native';
import Icon from './Icon';

interface CogWheelIconProps extends Icon{
    containerStyle?: any
}

const CogWheelIcon : React.FC<CogWheelIconProps> = (props: CogWheelIconProps) => {
    return (
        <View {...props.containerStyle}>
            <Svg width={props.width || 40} height={props.height || 40} viewBox="0 0 26 26" fill="none">
                <Path d="M12.9999 16.272C14.9347 16.272 16.5032 14.8068 16.5032 12.9993C16.5032 11.1918 14.9347 9.72656 12.9999 9.72656C11.0651 9.72656 9.49658 11.1918 9.49658 12.9993C9.49658 14.8068 11.0651 16.272 12.9999 16.272Z" stroke="#3686FF" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                <Path d="M22.3422 13C22.3321 12.2625 22.2206 11.5292 22.0105 10.8182L25 8.41818L23.2483 5.58182L19.5605 6.79491C18.4218 5.75343 17.0162 5.00221 15.478 4.61309L14.7517 1H11.2483L10.522 4.62182C8.9838 5.01094 7.57817 5.76215 6.43947 6.80364L2.75165 5.58182L1 8.41818L3.98949 10.8182C3.7794 11.5292 3.66794 12.2625 3.65784 13C3.66794 13.7375 3.7794 14.4708 3.98949 15.1818L1 17.5818L2.75165 20.4182L6.43947 19.2073C7.57817 20.2488 8.9838 21 10.522 21.3891L11.2483 25H14.7517L15.478 21.3782C17.0162 20.9891 18.4218 20.2378 19.5605 19.1964L23.2483 20.4073L25 17.5709L22.0105 15.1818C22.2206 14.4708 22.3321 13.7375 22.3422 13V13Z" stroke="#3686FF" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            </Svg>
        </View>
    )
}

export default CogWheelIcon;
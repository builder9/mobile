import React from 'react';
import { View } from 'react-native'
import Svg, {Path} from 'react-native-svg';
import Icon from './Icon';

interface FullStarIconProps extends Icon {
    containerStyle?: any
}

const FullStarIcon: React.FC<FullStarIconProps> = (props: FullStarIconProps) => {
    return (
        <View {...props.containerStyle}>
            <Svg width={props.width || 40} height={props.height || 40} viewBox="0 0 14 11" fill="none">
                <Path d="M7.00034 0L8.43723 4.1459H13.0871L9.32528 6.7082L10.7622 10.8541L7.00034 8.2918L3.23852 10.8541L4.67541 6.7082L0.91358 4.1459H5.56345L7.00034 0Z" fill="#6FA8FF" />
            </Svg>
        </View>
    )
}

export default FullStarIcon;
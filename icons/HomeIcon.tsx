import React from 'react';
import Svg, {Path} from 'react-native-svg';
import { View, ViewProps } from 'react-native';
import Icon from './Icon';

interface HomeIconProps extends Icon {
    containerStyle?: any
}

const HomeIcon : React.FC<HomeIconProps> = (props: HomeIconProps) => {
    return (
        <View {...props.containerStyle}>
            <Svg width={props.width || 40} height={props.height || 40} viewBox="0 0 26 23" fill="none">
                <Path d="M5.5 10.208V21.9996H11.2251V16.4661H13" stroke="#3686FF" stroke-width="2" strokeWidth="2"/>
                <Path d="M20.5 10.208V21.9996H14.7749V16.4661H13" stroke="#3686FF" stroke-width="2" strokeWidth="2"/>
                <Path d="M5.5 10.2084V9.96672H3L12.8999 2L23 9.96672H20.5V10.2084" stroke="#3686FF" stroke-width="2" strokeWidth="2"/>
            </Svg>
        </View>
    )
}

export default HomeIcon;

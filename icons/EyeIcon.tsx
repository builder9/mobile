import React from 'react';
import { View, ViewProps } from 'react-native'
import Svg, {Path} from 'react-native-svg';
import Icon from './Icon';

interface EyeIconProps extends Icon {
    containerStyle?: any
}

const EyeIcon: React.FC<EyeIconProps> = (props: EyeIconProps) => {
    return (
        <View {...props.containerStyle}>
            <Svg width={props.width || 40} height={props.height || 40} viewBox="0 0 18 14" fill="none">
                <Path d="M1 7.11542C1 7.11542 4.2 1.5 9 1.5C13.8 1.5 17 7.11542 17 7.11542C17 7.11542 13.8 12.7308 9 12.7308C4.2 12.7308 1 7.11542 1 7.11542Z" stroke="#828282" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="square"/>
                <Path d="M8.99985 9.52184C10.3254 9.52184 11.3999 8.44433 11.3999 7.1152C11.3999 5.78607 10.3254 4.70859 8.99985 4.70859C7.67437 4.70859 6.59985 5.78607 6.59985 7.1152C6.59985 8.44433 7.67437 9.52184 8.99985 9.52184Z" stroke="#828282" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="square" />
            </Svg>
        </View>
    )
}

export default EyeIcon;
import React from 'react';
import { View } from 'react-native'
import Svg, {Path} from 'react-native-svg';
import Icon from './Icon';

interface EmptyStarIconProps extends Icon {
    containerStyle?: any
}

const EmptyStarIcon: React.FC<EmptyStarIconProps> = (props: EmptyStarIconProps) => {
    return (
        <View {...props.containerStyle}>
            <Svg width={props.width || 40} height={props.height || 40} viewBox="0 0 13 11" fill="none">
                <Path d="M6.59995 0L8.03684 4.1459H12.6867L8.92489 6.7082L10.3618 10.8541L6.59995 8.2918L2.83813 10.8541L4.27501 6.7082L0.513189 4.1459H5.16306L6.59995 0Z" fill="#CCE0FF" />
            </Svg>
        </View>
    )
}

export default EmptyStarIcon;
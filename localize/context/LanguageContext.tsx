import React, {createContext, useContext, useEffect, useState} from 'react';
import en from "../languages/en.json"
import hr from '../languages/hr.json'
import * as RNLocalize from 'react-native-localize';
import App from '../../App';

type LanguageContextType = {
    signIn: string;
}

const LanguageContext = React.createContext<LanguageContextType>({} as LanguageContextType);

const languageObject = {
    'en': en,
    'hr': hr
}

interface LanguageContextProviderProps {
    children: JSX.Element | React.ReactNode[]
}

export const LanguageContextProvider : React.FC<LanguageContextProviderProps> = (props: LanguageContextProviderProps) => {
    const [selectedLanguage, setSelectedLanguage] = useState('en')

    useEffect(() => {
        const currentLanguage = RNLocalize.findBestAvailableLanguage(Object.keys(languageObject));

        setSelectedLanguage(currentLanguage?.languageTag || 'en');


    }, [])


    const value = {
        ...languageObject[selectedLanguage as 'en' | 'hr']
    }
    return (
        <LanguageContext.Provider value={value}>
            {props.children}
        </LanguageContext.Provider>
    )
}

export const useTranslation= () => useContext(LanguageContext);
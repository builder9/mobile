import React, { useState } from 'react';
import { StyleSheet, Text, View, SafeAreaView, ActivityIndicator } from 'react-native';
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import LoginScreen from './src/screens/LoginScreen';
import HomeScreen from './src/screens/HomeScreen';
import {Provider} from 'react-redux';
import Store from './src/redux/Store';
import { createNativeStackNavigator, NativeStackNavigationProp } from '@react-navigation/native-stack';
import RegisterScreen from './src/screens/RegisterScreen';
import { LanguageContextProvider } from './localize/context/LanguageContext';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import {ThemeProvider, Input } from 'react-native-elements'
import CategoriesScreen from './src/screens/CategoriesScreen';
import SubcategoriesScreen from './src/screens/CompaniesScreen';
import FilterScreen from './src/screens/FilterScreen';
import ProfileScreen from './src/screens/ProfileScreen';
import CogWheelIcon from './icons/CogWheelIcon';
import CompaniesScreen from './src/screens/CompaniesScreen';

export type RootStackParamList = {
  Home: undefined;
  Login: undefined;
  Register: undefined;
  Categories: undefined;
  Companies: {
    title: string
  };
  Profile: undefined;
  Filter: undefined;
  Settings: undefined;
}

export type RootNavigationProp = NativeStackNavigationProp<RootStackParamList>;

declare global {
  namespace ReactNativePaper {
    interface ThemeColors {
      myOwnColor: string;
    }

    interface Theme {
      myOwnProperty: boolean;
    }
  }
}


const theme = {
};


export default function App() {
  const RootStack = createNativeStackNavigator<RootStackParamList>();

  return (
    <Provider store={Store}>
      <SafeAreaProvider >
        <ThemeProvider theme={theme}>
          <LanguageContextProvider>
            <NavigationContainer >
              <RootStack.Navigator initialRouteName="Login">
                <RootStack.Screen name="Login" component={LoginScreen} options={{
                  headerShown: false
                }}/>
                <RootStack.Screen name="Home" component={HomeScreen} />
                <RootStack.Screen name="Register" component={RegisterScreen} options={{
                  headerShown: false
                }}/>
                <RootStack.Screen name="Categories" component={CategoriesScreen} options={{
                  headerShown: false
                }}/>
                <RootStack.Screen name="Filter" component={FilterScreen} options={{
                  headerShown: false
                }}/>
                <RootStack.Screen name={'Profile'} component={ProfileScreen} options={{
                  headerRight: () => {
                      return <CogWheelIcon />
                  }
                }}/>
                <RootStack.Screen name={'Companies'} component={CompaniesScreen} initialParams={{ title: "Back"}} options={{
                  headerRight: () => {
                      return <CogWheelIcon width={30}/>
                  },
                  headerShadowVisible: false,
                  headerTransparent: true,
                  headerTintColor: '#3686FF',
                  headerTitleStyle: {
                      fontSize: 25,
                      fontWeight: '700'
                  },
                  headerBackImageSource: 32
              }}/>
              </RootStack.Navigator>
            </NavigationContainer>
          </LanguageContextProvider>
          </ThemeProvider>
      </SafeAreaProvider>
    </Provider>
  );
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loader: {
    marginBottom: '40% '
  }
});

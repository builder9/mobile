// categories
export const Categories: string[] = ['Gardening', 'Exterior construction', 'Interior construction', 'Service', 'Trasnport', 'Emergency interventions', 'Other']

// subcategories
export const GardeningSubcategories: string[] = ['Grass cutting', 'Planting flowers', 'Cutting hedges', 'Tree maintenance']
export const ExteriorConstructionSubcategories: string[] = ['Excavations', 'Bricklayer', 'Carpenter', 'Plasterer', 'Concreting', 'Demolition', 'Isolateers', 'Roof coverings', 'Metal construction', 'ALU and PVC construction']
export const InteriorConstructionSubcategories: string[] = ['Plumber', 'Eletrician']
export const ServiceSubcategories: string[] = ['']
export const TransportSubcategories: string[] = ['']
export const EmergencyInterventionsSubcategories: string[] = ['']
export const OtherSubcategories: string[] = ['']
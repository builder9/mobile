import React, {FC } from 'react';
import { StyleSheet, Text, View, TextInputProps, TouchableWithoutFeedback, ViewStyle, TextStyle } from 'react-native'
import EyeIcon from '../../icons/EyeIcon';
import CrossedEye from '../../icons/CrossedEyeIcon';
import { OutlinedInput } from '@material-ui/core';
import { Button, Input } from 'react-native-elements';
import SearchIcon from '../../icons/SearchIcon';
import { IconNode } from 'react-native-elements/dist/icons/Icon';

interface MakuInputProps {
    placeholder?: string
    label?: string
    width?: string | number
    height?: string | number
    onChangeText?: (text:string) => void
    containerStyle?: ViewStyle
    inputContainerStyle?: ViewStyle
    inputStyle?: TextStyle
    labelStyle?: TextStyle
    makuContainer?: ViewStyle
    leftIcon?: IconNode
    rightIcon?: IconNode
    leftIconContainerStyle?: ViewStyle
}


const MakuInput : FC<MakuInputProps> = (props: MakuInputProps) => {
    return(
        <View style={props.makuContainer}>
            <Input
                underlineColorAndroid="transparent"
                label={props.label}
                inputStyle={{...styles.input, ...props.inputStyle}}
                containerStyle={{...styles.container, ...props.containerStyle}}
                inputContainerStyle={{...styles.inputContainer, ...props.inputContainerStyle}}
                labelStyle={{...styles.label, ...props.labelStyle}}
                placeholder={props.placeholder}
                leftIcon={props.leftIcon}
                rightIcon={props.rightIcon}
                leftIconContainerStyle={props.leftIconContainerStyle}
                onChangeText={props.onChangeText}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    touchable: {
    },
    input: {
        color: '#717171',
        fontSize: 12,
        fontWeight: '600',
        borderRadius: 20,
        marginLeft: '2%'
    },
    container: {
        paddingBottom: 1,
        height: 75,
    },
    inputContainer: {
        elevation: 4,
        borderBottomWidth: 0,
        borderRadius: 20,
        marginBottom: 1,
        backgroundColor: 'white'
    },
    label: {
        color: "#BBBBBB",
        marginBottom: '1.5%',
        fontSize: 13,
        fontWeight: "100",
    }
});

export default MakuInput;
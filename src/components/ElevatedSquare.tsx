import React, {FC } from 'react';
import { StyleSheet, View, TouchableWithoutFeedback, ViewStyle } from 'react-native'

interface ElevatedSquareProps {
    onPress?: () => void
    containerStyle?: ViewStyle
    squareStyle?: ViewStyle
    children?: React.ReactChild | React.ReactChild[]
}


const ElevatedSquare : FC<ElevatedSquareProps> = (props: ElevatedSquareProps) => {
    return(
        <View style={{...styles.container, ...props.containerStyle}}>
            <TouchableWithoutFeedback onPress={props.onPress}>
                <View style={{...styles.square, ...props.squareStyle}}>
                    {props.children}
                </View>
            </TouchableWithoutFeedback>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 100,
        paddingBottom: '3%',
        paddingHorizontal: '5%'
    },
    square: {
        backgroundColor: 'white',
        height: '100%',
        borderRadius: 20,
        elevation: 5,
        shadowColor: '#277EFF',
    },
});

export default ElevatedSquare;
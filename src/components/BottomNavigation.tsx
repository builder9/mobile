import React, {FC } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Button, TextInputProps } from 'react-native'
import BellIcon from '../../icons/BellIcon';
import CalendarIcon from '../../icons/CalendarIcon';
import ChatIcon from '../../icons/ChatIcon';
import HomeIcon from '../../icons/HomeIcon';
import WrenchIcon from '../../icons/WrenchIcon';
import MakuIconButton from './MakuIconButton';

interface BottomNavigationProps {
    styles?: React.ReactNode
}


const BottomNavigation : FC<BottomNavigationProps> = (props: BottomNavigationProps) => {
    return (
        <View style={styles.container}>
            <MakuIconButton width={40} onPress={() => {}}>
                <WrenchIcon height={30} width={30}/>
            </MakuIconButton>
            <MakuIconButton width={40} onPress={() => {}}>
                <CalendarIcon height={30} width={30}/>
            </MakuIconButton>
            <View style={styles.circle}>
                <MakuIconButton width={40} onPress={() => {}}>
                    <HomeIcon height={36} width={30}/>
                </MakuIconButton>
            </View>
            <MakuIconButton width={40} onPress={() => {}}>
                <ChatIcon height={30} width={30}/>
            </MakuIconButton>
            <MakuIconButton width={40} onPress={() => {}}>
                <BellIcon height={30} width={30}/>
            </MakuIconButton>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    buttonText: {
        color: 'blue',
        fontWeight: '700'
    },
    circle: {
        elevation: 5,
        shadowColor: '#043073',
        backgroundColor: 'white',
        height: '100%',
        width: '17%',
        borderRadius: 90,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default BottomNavigation;
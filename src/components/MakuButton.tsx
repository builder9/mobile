import { useLinkProps } from '@react-navigation/native';
import React, {FC } from 'react';
import { StyleSheet, Text, View, ViewStyle, TextStyle, TouchableOpacity, ViewProps, TextProps } from 'react-native'

interface MakuButtonProps {
    viewStyle?: ViewProps | ViewStyle
    textStyle?: TextProps | TextStyle
    onPress: any;
    children: React.ReactNode
    height?: number | string
    width: number | String
}


const MakuButton : FC<MakuButtonProps> = (props: MakuButtonProps) => {
    return (
        <TouchableOpacity onPress={() => props.onPress()} >
            <View style={{...styles.button, ...props.viewStyle}} {...props}>
                <Text style={{...styles.buttonText, ...props.textStyle}}>{props.children}</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: '#3686FF',
        borderRadius: 15,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText: {
        color: 'white',
        fontWeight: '700'
    }
});

export default MakuButton;
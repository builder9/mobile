import React, {FC } from 'react';
import { StyleSheet, Text, View, TextInputProps, TouchableWithoutFeedback, TouchableNativeFeedback, TouchableNativeFeedbackBase } from 'react-native'
import EyeIcon from '../../icons/EyeIcon';
import CrossedEye from '../../icons/CrossedEyeIcon';
import { OutlinedInput } from '@material-ui/core';
import { Button, Input, Icon } from 'react-native-elements';
import CrossedEyeIcon from '../../icons/CrossedEyeIcon';

interface MakuPasswordInputProps {
    placeholder: string
    label: string
    width: string | number
    height: string | number
    onChangeText: (text:string) => void
}


const MakuPasswordInput : FC<MakuPasswordInputProps> = (props: MakuPasswordInputProps) => {
    const [show, setShow] = React.useState(true)

    return(
        <Input
            secureTextEntry={show}
            onFocus={e => {
                console.log('hello')
            }}
            label={props.label}
            rightIcon={
                <Button buttonStyle={styles.eyeButton} onPress={() => setShow(!show)} icon={
                    show ? <EyeIcon height={20} width={20} containerStyle={styles.iconContainer}/>
                    : <CrossedEyeIcon height={20} width={20} containerStyle={styles.iconContainer}/>
                }/>
            }
            inputStyle={styles.input}
            inputContainerStyle={styles.inputContainerStyle}
            labelStyle={styles.label}
            placeholder={props.placeholder}
            containerStyle={styles.containerStyle}
            onChangeText={props.onChangeText}
        />
    )
}

const styles = StyleSheet.create({
    eyeButton: {
        padding: "1%",
        backgroundColor: "transparent"
    },
    iconContainer: {
    },
    input: {
        color: '#717171',
        fontSize: 12,
        fontWeight: '600',
        borderRadius: 20,
        marginLeft: "4%"
    },
    containerStyle: {
        marginTop: "2%",
        alignSelf: 'stretch',
    },
    inputContainerStyle: {
        elevation: 4,
        backgroundColor: 'white',
        borderBottomWidth: 0,
        borderRadius: 20,
        marginBottom: 1,
        alignSelf: 'stretch',
    },
    label: {
        color: "#BBBBBB",
        marginBottom: '1.5%',
        fontSize: 13,
        marginLeft: '5%',
        fontWeight: "100"
    }
});

export default MakuPasswordInput;
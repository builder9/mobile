import { useLinkProps } from '@react-navigation/native';
import React, {FC } from 'react';
import { StyleSheet, Text, View, ViewStyle, TextStyle, TouchableOpacity, ViewProps, TextProps, TouchableWithoutFeedback } from 'react-native'
import Icon from '../../icons/Icon';

interface MakuIconButtonProps {
    viewStyle?: ViewProps | ViewStyle
    textStyle?: TextProps | TextStyle
    onPress: any;
    children: React.ReactNode
    height?: number | string
    width: number | String
}


const MakuIconButton : FC<MakuIconButtonProps> = (props: MakuIconButtonProps) => {
    return (
        <TouchableWithoutFeedback onPress={() => props.onPress()} >
            <View style={{...styles.button, ...props.viewStyle}} {...props}>
                {props.children}
            </View>
        </TouchableWithoutFeedback>
    )
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: 'pink',
        borderRadius: 15,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText: {
        color: 'white',
        fontWeight: '700'
    }
});

export default MakuIconButton;
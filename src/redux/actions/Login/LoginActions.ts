import { NavigationProp, useNavigation } from "@react-navigation/native"
import { NativeStackNavigationProp } from "@react-navigation/native-stack/lib/typescript/src/types"
import axios from "axios"
import { Dispatch } from "redux"
import { RootNavigationProp, RootStackParamList } from "../../../../App"
import { LoginDispatchTypes, LOGIN_FAIL, LOGIN_LOADING, LOGIN_SUCCESS } from "./LoginActionTypes"

const something = {
    email: 'denis@builder.com',
    password: 'denis'
}

export const Login = (
        email: string,
        password: string,
    ) => async (dispatch: Dispatch<LoginDispatchTypes>) => {

    

    console.log('it was called')
    try {
        dispatch({
            type: LOGIN_LOADING
        })

        console.log('the email: ', email)
        console.log('the password: ', password)

        const loginRequest = {
            Email: email,
            Password: password
        }

        const res = await axios.post("http://a658-93-137-238-166.ngrok.io/api/auth/login", loginRequest, undefined)

        console.log('the response: ', res.data)

        dispatch({
            type: LOGIN_SUCCESS,
            payload: res.data
        })
        console.log('hello')

    } catch(e) {
        dispatch({
            type: LOGIN_FAIL
        })
    }

} 
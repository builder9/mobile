import { NativeStackNavigationProp } from "@react-navigation/native-stack/lib/typescript/src/types"
import axios from "axios"
import { Dispatch } from "redux"
import { RootStackParamList } from "../../../../App"
import { RegisterDispatchTypes, REGISTER_LOADING, REGISTER_FAIL, REGISTER_SUCCESS } from "./RegisterActionTypes"
import { RegisterFormData } from '../../../containers/RegisterForm';


const something = {
    firstname: 'denis',
    lastname: 'notdenis',
    email: 'denis@builder.com',
    password: 'denis'
}

export const Register = (
        data: RegisterFormData,
        navigation: NativeStackNavigationProp<RootStackParamList, 'Home'>
    ) => async (dispatch: Dispatch<RegisterDispatchTypes>) => {

    console.log('it was called')
    try {
        dispatch({
            type: REGISTER_LOADING
        })

        const registerRequest = {
            FirstName: data.firstname,
            LastName: data.lastname,
            Email: data.email,
            Password: data.password
        }

        const res = await axios.post("http://a493-93-143-226-140.ngrok.io/api/auth/login", registerRequest, undefined)

        dispatch({
            type: REGISTER_SUCCESS,
            payload: res.data
        })
        navigation.navigate('Home')
    } catch(e) {
        dispatch({
            type: REGISTER_FAIL
        })
    }

} 
export const REGISTER_LOADING = "REGISTER_LOADING";
export const REGISTER_FAIL = "REGISTER_FAIL";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";

export type Register = {
    userId: string
    token: string
    refreshToken: string
}

export interface RegisterLoading {
    type: typeof REGISTER_LOADING
}

export interface RegisterFail {
    type: typeof REGISTER_FAIL
}

export interface RegisterSuccess {
    type: typeof REGISTER_SUCCESS
    payload: Register
}


export type RegisterDispatchTypes = RegisterLoading | RegisterFail | RegisterSuccess



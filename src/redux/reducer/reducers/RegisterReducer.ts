import { Register, RegisterDispatchTypes, REGISTER_FAIL, REGISTER_LOADING, REGISTER_SUCCESS } from "../../actions/Register/RegisterActionTypes";

interface DefaultState {
    loading: boolean,
    register?: Register
}


const defaultState: DefaultState = {
    loading: false
};



const loginReducer = (state: DefaultState = defaultState, action: RegisterDispatchTypes) => {
    switch (action.type) {
        case REGISTER_LOADING:
            return {
                ...state,
                loading: true
            }
        case REGISTER_FAIL:
            return {
                ...state,
                loading: false
            }
        case REGISTER_SUCCESS:
            return {
                ...state,
                loading: false,
                login: action.payload
            }
        default:
            return state
    }

    return state
}

export default loginReducer;
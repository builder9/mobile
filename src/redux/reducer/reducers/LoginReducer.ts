import { Login, LoginDispatchTypes, LOGIN_FAIL, LOGIN_LOADING, LOGIN_SUCCESS } from "../../actions/Login/LoginActionTypes";

interface DefaultState {
    loading: boolean,
    login?: Login
}


const defaultState: DefaultState = {
    loading: false
};



const loginReducer = (state: DefaultState = defaultState, action: LoginDispatchTypes) => {
    switch (action.type) {
        case LOGIN_LOADING:
            return {
                ...state,
                loading: true
            }
        case LOGIN_FAIL:
            return {
                ...state,
                loading: false
            }
        case LOGIN_SUCCESS:
            return {
                ...state,
                loading: false,
                login: action.payload
            }
        default:
            return state
    }

    return state
}

export default loginReducer;
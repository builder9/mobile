import { combineReducers } from "redux";
import loginReducer from "./reducers/LoginReducer";

const RootReducer = combineReducers({
    login: loginReducer
})

export default RootReducer;
import React, {FC } from 'react';
import { StyleSheet, Text, View } from 'react-native'

interface ProfileScreenProps {
}


const ProfileScreen : FC<ProfileScreenProps> = (props: ProfileScreenProps) => {

    // const {signIn} = useTranslation();
    // const navigation = useNavigation<RootNavigationProp>();
    return(
        <View style={styles.screen}>
            <Text>Profile</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FDFEFF',
        alignSelf: 'stretch',
    },
})

export default ProfileScreen;
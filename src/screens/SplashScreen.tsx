import { LinearGradient } from 'react-native-linear-gradient';
import React, {FC } from 'react';
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native'

interface SplashScreenProps {
    fontLoaded: boolean
}


const SplashScreen : FC<SplashScreenProps> = (props: SplashScreenProps) => {

    return(
        <View style={styles.screen}>
            <Text>Hello</Text>
            {/* <LinearGradient colors={['#84B5FF', '#0955C7' ]}
             style={styles.linearGradient}
            //  start={[0.9, 0]}
             locations={[0, 1]}>
                <View style={styles.container}>
                    <Text style={styles.text}>MAKU</Text>
                    <ActivityIndicator style={styles.loader} size="large" color="#FFFFFF" animating={props.fontLoaded} />
                </View>
            </LinearGradient> */}

        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontSize: 50,
        color: 'white'
    },
    linearGradient: {
        width: '100%',
        height: '100%'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    loader: {
        marginTop: '2%'
    }
})

export default SplashScreen;
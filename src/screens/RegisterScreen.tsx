import React, {FC } from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native'
import MakuButton from '../components/MakuButton';
import MakuInput from '../components/MakuInput';
import  LinearGradient  from 'react-native-linear-gradient';
//import axios, {AxiosResponse} from 'axios';
import { NativeStackNavigationProp, NativeStackScreenProps } from '@react-navigation/native-stack';
import {LoginForm} from '../containers/LoginForm';
import { Control, UseFormHandleSubmit, UseFormRegister } from 'react-hook-form/dist/types/form';
import { FieldValues } from 'react-hook-form/dist/types/fields';
import { Controller, useForm } from 'react-hook-form';
import { RootStackParamList } from '../../App';
import { useTranslation } from '../../localize/context/LanguageContext';
import  RegisterForm from '../containers/RegisterForm';

type RegisterScreenProps = NativeStackScreenProps<RootStackParamList, 'Home'>


const RegisterScreen : FC<RegisterScreenProps> = (props: RegisterScreenProps) => {

    const {control, handleSubmit, setError, formState: {errors} } = useForm<FormData>();

    return(
        <View style={styles.screen}>
            <View style={styles.container}>
                <Text style={styles.text}>Sign up</Text>
                <RegisterForm navigation={props.navigation}/>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        backgroundColor: '#FDFEFF'
    },
    text: {
        color: '#3686FF',
        fontSize: 65,
        fontWeight: '700'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        paddingTop: '15%'
    },
    topContainer: {
        width: '100%',
        height: '40%',
        paddingBottom: '7%',
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: 'green'
    }
});

export default RegisterScreen;
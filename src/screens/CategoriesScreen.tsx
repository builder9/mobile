import React, {FC } from 'react';
import { StyleSheet, Text, View, TextInput, Button, ScrollView } from 'react-native'
import MakuButton from '../components/MakuButton';
import MakuInput from '../components/MakuInput';
import CategorySquare from '../components/ElevatedSquare';
import { useState } from 'react';
import BottomNavigation from '../components/BottomNavigation';
import CogWheelIcon from '../../icons/CogWheelIcon';
import { Categories } from '../util/constants';
import { CategorySquareContainer } from '../containers/CategorySquareContainer';


export interface CategoriesScreenNavigationProps {

}

const CategoriesScreen : FC<CategoriesScreenNavigationProps> = (props: CategoriesScreenNavigationProps) => {

    return(
        <View style={styles.screen}>
            <View style={styles.navbar}>
                <View>
                    <Text style={styles.headerText}>Categories</Text>
                </View>
                <View>
                    <CogWheelIcon />
                </View>
            </View>
            <ScrollView contentContainerStyle={styles.scrollViewContent} style={styles.scrollView}>
                {
                    Categories && Categories.map((category: string, index: number) => {
                        return(
                            <CategorySquareContainer category={category} key={index} />
                        )
                    })
                }
            </ScrollView>
            <View style={styles.bottomContainer}>
                <BottomNavigation />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
    },
    scrollViewContent: {

    },
    scrollView: {

    },
    headerText: {
        color: '#3686FF',
        fontSize: 32,
        fontWeight: '700'
    },
    bottomContainer: {
        height: '8%'
    },
    navbar: {
        width: '100%',
        height: '10%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: '5%',
        marginBottom: '5%'
    }
});

export default CategoriesScreen;
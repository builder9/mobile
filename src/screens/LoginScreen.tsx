import React, {FC } from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native'
import MakuButton from '../components/MakuButton';
import MakuInput from '../components/MakuInput';
import  LinearGradient  from 'react-native-linear-gradient';
//import axios, {AxiosResponse} from 'axios';
import { NativeStackNavigationProp, NativeStackScreenProps } from '@react-navigation/native-stack';
import {LoginForm} from '../containers/LoginForm';
import { Control, UseFormHandleSubmit, UseFormRegister } from 'react-hook-form/dist/types/form';
import { FieldValues } from 'react-hook-form/dist/types/fields';
import { Controller, useForm } from 'react-hook-form';
import { RootNavigationProp, RootStackParamList } from '../../App';
import { useTranslation } from '../../localize/context/LanguageContext';
import { Link, useNavigation } from '@react-navigation/native';

interface LogingScreenProps {

}


const LoginScreen : FC<LogingScreenProps> = (props: LogingScreenProps) => {

    const {signIn} = useTranslation();
    const navigation = useNavigation<RootNavigationProp>();
    return(
        <View style={styles.screen}>
            <View style={styles.container} >
                <Text style={styles.headerText}>{signIn}</Text>
                <LoginForm navigation={navigation}/>
                <Text style={styles.text}>Don't have an account? <Link style={styles.link} to={{screen: 'Register'}}>Sign up</Link></Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FDFEFF',
        alignSelf: 'stretch',
    },
    link: {
        color: '#3686FF',
        fontWeight: 'bold',
        textDecorationLine: 'underline'
    },
    text: {
        paddingTop: '40%',
        color: '#9F9F9F',
        fontWeight: '500',
    },
    headerText: {
        color: '#3686FF',
        fontSize: 65,
        fontWeight: '700',
    },
    container: {
        flexGrow: 1,
        height: '100%',
        width: '100%',
        alignItems: 'center',
        paddingTop: '35%',
    },
});

export default LoginScreen;
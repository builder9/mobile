import React, {FC } from 'react';
import { StyleSheet, Text, View, TextInput, Button, ScrollView } from 'react-native'
import { NavigationProp, NavigationState, RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import { CategoriesStackParamList } from '../../navigation/CategoriesStack';
import { NavigationScreenProp } from 'react-navigation';
import { NativeStackNavigationOptions } from '@react-navigation/native-stack/lib/typescript/src/types';
import CompanySquareContainer from '../containers/CompanySquareContainer';
import { useHeaderHeight } from '@react-navigation/elements';
import SearchIcon from '../../icons/SearchIcon';
import MakuButton from '../components/MakuButton';
import { CheckBox } from 'react-native-elements/dist/checkbox/CheckBox';

type FilterScreenNavigationProps = NavigationScreenProp<FilterParams, CategoriesStackParamList>


interface FilterParams extends NavigationState {
    title: string
}


const FilterScreen : FC<FilterScreenNavigationProps> = (props: FilterScreenNavigationProps) => {
    // when the header is transparent it becomes absolute so this is needed to keep the other components under it
    const headerHeight = useHeaderHeight()
    let Companies = ['Comp one', 'Comp two', 'Comp three']
    const navigation = useNavigation<NavigationProp<CategoriesStackParamList, 'Companies'>>();
    const route = useRoute<RouteProp<CategoriesStackParamList, 'Companies'>>();
    const searchIcon = <SearchIcon width={20} height={20}/>


    return(
        <View style={styles.screen}>
            <View style={styles.headerContainer}>
                <Text style={styles.headerText}>Filter</Text>
            </View>
            <View style={styles.mainContainer}>
                <Text>Location</Text>
                <Text>Specialty</Text>
                <Text>Stars</Text>
                <CheckBox />
                <MakuButton onPress={() => console.log('apply filters')} width={"80%"}>Apply</MakuButton>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        height: '90%',
        justifyContent: 'space-between'
    },
    headerText: {
        fontSize: 20,
        fontWeight: '800'
    },
    headerContainer: {
        height: '10%',
        backgroundColor: 'blue'
    },
    screen: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        backgroundColor: 'white'
    },
});

export default FilterScreen;
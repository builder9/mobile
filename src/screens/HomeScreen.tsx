import React, {FC } from 'react';
import { StyleSheet, Text, View, TextInput, Button, ScrollView } from 'react-native'
import MakuButton from '../components/MakuButton';
import MakuInput from '../components/MakuInput';
import CategorySquare from '../components/ElevatedSquare';
import { useState } from 'react';
import BottomNavigation from '../components/BottomNavigation';

interface HomeScreenProps {

}


const HomeScreen : FC<HomeScreenProps> = (props: HomeScreenProps) => {

    const [categories, setCategories] = useState([
        { text: 'Ceramics', key: 1},
        { text: 'Locksmith', key: 2},
        { text: 'Electrician', key: 3},
        { text: 'Plumber', key: 4},
        { text: 'Roof', key: 5},
        { text: 'Applicances', key: 6},
        { text: 'Windows', key: 7},
        { text: 'Interior', key: 8},
    ]);

    return(
        <View style={styles.screen}>
            <Text>Welcome to MAKU your construction buddy</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    searchInput: {
        width: '90%',
        height: '25%',
        backgroundColor: 'blue',
        borderRadius: 15,
        paddingHorizontal: '3%'
    },
    bottomContainer: {
        height: '8%',
        width: '100%',
    },
    grid: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    scrollView: {
        flex: 1
    },
    scrollViewContent: {
        justifyContent: 'flex-start',
        flexDirection: 'column'
    },
    text: {
        color: 'white',
    },
    headerText: {
        color: 'blue',
        fontSize: 32,
        fontWeight: '700'
    },
    topContainerBottomContainer: {
        paddingTop: '12%',
        flex: 1,
        width: '100%',
        alignItems: 'center',
    },
    topContainer: {
        width: '100%',
        height: '30%',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    topContainerTopRow: {
        width: '100%',
        height: '30%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: '5%'
    }
});

export default HomeScreen;
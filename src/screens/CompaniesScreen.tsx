import React, {FC } from 'react';
import { StyleSheet, Text, View, TextInput, Button, ScrollView } from 'react-native'
import MakuButton from '../components/MakuButton';
import MakuInput from '../components/MakuInput';
import CategorySquare from '../components/ElevatedSquare';
import { useState } from 'react';
import BottomNavigation from '../components/BottomNavigation';
import CogWheelIcon from '../../icons/CogWheelIcon';
import { Categories } from '../util/constants';
import { CategorySquareContainer } from '../containers/CategorySquareContainer';
import { NavigationProp, NavigationState, RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import { NavigationScreenProp } from 'react-navigation';
import { NativeStackNavigationOptions } from '@react-navigation/native-stack/lib/typescript/src/types';
import CompanySquareContainer from '../containers/CompanySquareContainer';
import { useHeaderHeight } from '@react-navigation/elements';
import SearchIcon from '../../icons/SearchIcon';
import { RootStackParamList } from '../../App';

type CompaniesScreenNavigationProps = NavigationScreenProp<CompaniesParams, RootStackParamList>


interface CompaniesParams extends NavigationState {
    title: string
}


const CompaniesScreen : FC<CompaniesScreenNavigationProps> = (props: CompaniesScreenNavigationProps) => {
    // when the header is transparent it becomes absolute so this is needed to keep the other components under it
    const headerHeight = useHeaderHeight()
    let Companies = ['Comp one', 'Comp two', 'Comp three']
    const navigation = useNavigation<NavigationProp<RootStackParamList, 'Companies'>>();
    const route = useRoute<RouteProp<RootStackParamList, 'Companies'>>();
    const searchIcon = <SearchIcon width={20} height={20}/>


    navigation.setOptions({ headerTitle: route.params.title})
    return(
        <View style={{paddingTop: headerHeight, ...styles.screen}}>
            <MakuInput makuContainer={styles.makuContainer} placeholder={"Search"} leftIcon={searchIcon} leftIconContainerStyle={styles.leftIconContainerStyle}/>
            <ScrollView contentContainerStyle={styles.scrollViewContent} style={styles.scrollView}>
                {
                    Companies && Companies.map((company, index) => {
                        return (
                            <CompanySquareContainer />
                        )
                    })
                }
            </ScrollView>
            <View style={styles.bottomContainer}>
                <BottomNavigation />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    leftIconContainerStyle: {
        marginLeft: '3%',
        padding: 0,
        paddingRight: 0,
    },
    makuContainer: {
        marginTop: '5%'
    },
    screen: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        backgroundColor: 'white'
    },
    scrollViewContent: {

    },
    scrollView: {

    },
    bottomContainer: {
        height: '8%'
    },
    navbar: {
        width: '100%',
        height: '10%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: '5%',
        marginBottom: '5%'
    }
});

export default CompaniesScreen;
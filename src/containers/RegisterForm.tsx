import React, {FC, useState } from 'react';
import { StyleSheet, Text, View, TextInput, Button, ScrollView } from 'react-native'
import { useForm, Controller, FieldError, useFormContext } from 'react-hook-form';
import axios, {AxiosResponse, AxiosError} from 'axios';
import { useDispatch } from 'react-redux';
import MakuInput from '../components/MakuInput';
import MakuButton from '../components/MakuButton';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { RootStackParamList } from '../../App';
import MakuPasswordInput from '../components/MakuPasswordInput';
import { Register } from '../redux/actions/Register/RegisterActions';
import { CheckBox } from 'react-native-elements';
import { Link } from '@react-navigation/native';
import MakuLinkCheckBox from './MakuLinkCheckBox';


interface RegisterFormProps {
    navigation: NativeStackNavigationProp<RootStackParamList, 'Home'>
}


type LoginResponse = {
    userId: string,
    token: string,
    refreshToken: string
}


export type RegisterFormData = {
    firstname: string;
    lastname: string;
    email: string;
    password: string;
}


const RegisterForm: FC<RegisterFormProps>  = (props: RegisterFormProps) => {
    const dispatch = useDispatch();
    const [showTitle, setShowTitle] = useState(true);

    const {control, handleSubmit, setError, formState: {errors} } = useForm<RegisterFormData>();

    const succSubmit = (data:RegisterFormData) => {
        dispatch(Register(data, props.navigation))
    }

    const something = handleSubmit(succSubmit, (error) => console.log('the erros: ', error))

    console.log('hello darkness')

    return (
        <View style={styles.container}>
            <View style={styles.firstnameContainer}>
                <Controller
                    control={control}
                    name={"firstname"}
                    rules={{ required: {
                        value: true,
                        message: 'This field is required'
                    }}}
                    render={({ field: {onChange, value}}) => {
                        return <MakuInput label={"Firstname"} onChangeText={value => onChange(value)} placeholder="" height={50} width={"100%"}/>
                    }} 
                    defaultValue=""
                />
                {errors.email && <Text>This is required</Text>}
            </View>
            <View style={styles.lastnameContainer}>
                <Controller
                    control={control}
                    name={"lastname"}
                    rules={{ required: {
                        value: true,
                        message: 'This field is required'
                    }}}
                    render={({ field: {onChange, value}}) => {
                        return <MakuInput label={"Lastname"} onChangeText={value => onChange(value)} placeholder="" height={50} width={"100%"}/>
                    }} 
                    defaultValue=""
                />
                {errors.email && <Text>This is required</Text>}
            </View>
            <View style={styles.emailContainer}>
                <Controller
                    control={control}
                    name={"email"}
                    rules={{ required: {
                        value: true,
                        message: 'This field is required'
                    }}}
                    render={({ field: {onChange, value}}) => {
                        return <MakuInput label={"E-mail"} onChangeText={value => onChange(value)} placeholder="" height={50} width={"100%"}/>
                    }} 
                    defaultValue=""
                />
                {errors.email && <Text>This is required</Text>}
            </View>
            <View style={styles.passwordContainer}>
                <Controller 
                    control={control}
                    name={"password"}
                    rules={{ required: {
                        value: true,
                        message: 'This field is required'
                    }, }}
                    render={({ field: {onChange, onBlur, value}}) => {
                        return <MakuPasswordInput label={"Password"} onChangeText={value => onChange(value)} placeholder="" height={50} width={"100%"}/>
                    }}
                    defaultValue=""
                />
                {errors.email && <Text>This is required</Text>}
            </View>
            <MakuLinkCheckBox navigation={props.navigation}/>
            <MakuButton viewStyle={styles.signButton} onPress={something} width={220} height={50}>Sign up</MakuButton>
            <MakuButton viewStyle={styles.cancelButton} textStyle={styles.cancelButtonText} onPress={() => props.navigation.navigate('Login')} width={180} height={50}>Cancel</MakuButton>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        alignSelf: 'stretch',
        height: '100%',
        alignItems: 'center',
    },
    checkboxContainer: {
        backgroundColor: 'transparent',
        borderWidth: 0,
    },
    text: {
        paddingTop: '5%',
        color: '#3686FF',
        fontWeight: '600'
    },
    emailContainer: {
        paddingTop: '10%',
        paddingLeft: '3%',
        paddingRight: '3%',
        alignSelf: 'stretch',
    },
    passwordContainer: {
        paddingLeft: '3%',
        paddingRight: '3%',
        alignSelf: 'stretch'
    },
    controllerContainer: {
        width: '100%',
        backgroundColor: '#3686FF'
    },
    firstnameContainer: {
        marginTop: '10%',
        height: 50,
        paddingLeft: '3%',
        paddingRight: '3%',
        alignSelf: 'stretch'
    },
    lastnameContainer: {
        height: 50,
        marginTop: '10%',
        paddingLeft: '3%',
        paddingRight: '3%',
        alignSelf: 'stretch'
    },
    cancelButton: {
        marginTop: '2%',
        backgroundColor: 'transparent',
        borderWidth: 3,
        borderColor: '#3686FF',
        justifyContent: 'center',
        alignItems: 'center',
    },
    cancelButtonText: {
        color: "#3686FF"
    },
    signButton: {
        marginTop: '15%'
    }
  });


  export default RegisterForm;
import React, {FC } from 'react';
import { StyleSheet, Text, View, TextInput, Button, ScrollView } from 'react-native'
import ElevatedSquare from '../components/ElevatedSquare';
import { NavigationProp, useNavigation } from '@react-navigation/native';
import { RootStackParamList } from '../../App';


interface CategorySquareContainerProps {
    category: string
}

export const CategorySquareContainer: FC<CategorySquareContainerProps>  = (props: CategorySquareContainerProps) => {
    const navigation = useNavigation<NavigationProp<RootStackParamList, 'Companies'>>();

    const onPressHandler = () => {
        console.log('bye')
        console.log('the: ', props.category)
        navigation.navigate('Companies', { title: props.category})
    }

    return (
        <View>
            <ElevatedSquare onPress={onPressHandler} containerStyle={styles.elevatedSquareContainer} squareStyle={styles.elevatedSquare}> 
                <View>
                    <Text style={styles.text}>{props.category}</Text>
                </View>
            </ElevatedSquare>
        </View>
    )
}


const styles = StyleSheet.create({
    elevatedSquareContainer: {
        height: 180,
        paddingBottom: '3%',
        paddingHorizontal: '5%'
    },
    elevatedSquare: {
        justifyContent: 'flex-end',
        padding: '3%'
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'stretch',
    },
    text: {
        color: '#3686FF',
        fontWeight: '700',
        fontSize: 20
    },
  });

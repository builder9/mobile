import React, {FC, useState } from 'react';
import { StyleSheet, Text, View, TextInput, Button, ScrollView } from 'react-native'
import { useForm, Controller, FieldError, useFormContext } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import MakuInput from '../components/MakuInput';
import MakuButton from '../components/MakuButton';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { RootStackParamList } from '../../App';
import MakuPasswordInput from '../components/MakuPasswordInput';
import { CheckBox } from 'react-native-elements';
import { Link } from '@react-navigation/native';
import { Screen } from 'react-native-screens';
import UncheckedCheckBoxIcon from '../../icons/UncheckedCheckBoxIcon';
import CheckedCheckBoxIcon from '../../icons/CheckedCheckBoxIcon';
import EyeIcon from '../../icons/EyeIcon';

interface MakuLinkCheckBoxProps {
    navigation: NativeStackNavigationProp<RootStackParamList, 'Home'>
}


type LoginResponse = {
    userId: string,
    token: string,
    refreshToken: string
}


type FormData = {
    email: string;
    password: string;
}

const MakuLinkCheckBox: FC<MakuLinkCheckBoxProps>  = (props: MakuLinkCheckBoxProps) => {
    const [checked, setChecked] = useState(false)

    return (
        <View style={styles.container}>
            <CheckBox 
                uncheckedIcon={<UncheckedCheckBoxIcon width={25} height={25}/>} 
                checkedIcon={<CheckedCheckBoxIcon width={25} height={25}/>} 
                onPress={() => setChecked(!checked)} 
                checked={checked}/>
            <View>
                <Text style={styles.text}>By signing up you accept the 
                <Link style={styles.link} to={{screen: 'Home'}}> Terms of service</Link>
                <Text style={styles.text}> and </Text>
                <Link style={styles.link} to={{screen: 'Home'}}>Privacy Policy</Link>
                </Text>
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '60%',
        flexDirection: 'row',
    },
    emailContainer: {
        paddingTop: '10%',
        paddingLeft: '10%',
        paddingRight: '10%',
        alignSelf: 'stretch',
    },
    passwordContainer: {
        paddingLeft: '10%',
        paddingRight: '10%',
        alignSelf: 'stretch'
    },
    controllerContainer: {
        width: '100%',
        backgroundColor: '#3686FF'
    },
    link: {
        color: '#3686FF',
        fontWeight: '600',
        textDecorationLine: 'underline'
    },
    text: {
        color: '#717171'
    }
  });


  export default MakuLinkCheckBox;
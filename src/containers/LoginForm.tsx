import React, {FC } from 'react';
import { StyleSheet, Text, View, TextInput, Button, ScrollView } from 'react-native'
import { useForm, Controller, FieldError, useFormContext } from 'react-hook-form';
import axios, {AxiosResponse, AxiosError} from 'axios';
import {Login} from '../redux/actions/Login/LoginActions';
import { useDispatch } from 'react-redux';
import MakuInput from '../components/MakuInput';
import MakuButton from '../components/MakuButton';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { RootNavigationProp, RootStackParamList } from '../../App';
import MakuPasswordInput from '../components/MakuPasswordInput';
import { NavigationProp, useNavigation } from '@react-navigation/native';


interface LoginFormProps {
    navigation: RootNavigationProp
}


type LoginResponse = {
    userId: string,
    token: string,
    refreshToken: string
}


type FormData = {
    email: string;
    password: string;
}



export const LoginForm: FC<LoginFormProps>  = (props: LoginFormProps) => {

    const dispatch = useDispatch();
    // const [login, setLogin] = useState<LoginResponse>()

    const {control, handleSubmit, setError, formState: {errors} } = useForm<FormData>();
    const navigation = useNavigation<NavigationProp<RootStackParamList, 'Categories'>>();

    const succSubmit = (data:FormData) => {
        console.log('the data ', data)
        dispatch(Login(data.email, data.password))

        navigation.navigate('Categories');
    }

    const something = handleSubmit(succSubmit, (error) => console.log('the erros: ', error))

    return (
        <View style={styles.container}>
            <View style={styles.emailContainer}>
                <Controller
                    control={control}
                    name={"email"}
                    rules={{ required: {
                        value: true,
                        message: 'This field is required'
                    }}}
                    render={({ field: {onChange, value}}) => {
                        return <MakuInput label={"E-mail"} onChangeText={value => onChange(value)} placeholder="" height={50} width={"100%"}/>
                    }} 
                    defaultValue=""
                />
                {errors.email && <Text>This is required</Text>}
            </View>
            <View style={styles.passwordContainer}>
                <Controller 
                    control={control}
                    name={"password"}
                    rules={{ required: {
                        value: true,
                        message: 'This field is required'
                    }, }}
                    render={({ field: {onChange, onBlur, value}}) => {
                        return <MakuPasswordInput label={"Password"} onChangeText={value => onChange(value)} placeholder="" height={50} width={"100%"}/>
                    }}
                    defaultValue=""
                />
                {errors.email && <Text>This is required</Text>}
            </View>
            <MakuButton onPress={something} width={150} height={45}>Sign In</MakuButton>
            <Text style={styles.text}>Forgot password?</Text>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'stretch',
    },
    text: {
        paddingTop: '5%',
        color: '#3686FF',
        fontWeight: '600'
    },
    emailContainer: {
        paddingTop: '10%',
        paddingLeft: '3%',
        paddingRight: '3%',
        alignSelf: 'stretch',
    },
    passwordContainer: {
        paddingLeft: '3%',
        paddingRight: '3%',
        alignSelf: 'stretch'
    },
    controllerContainer: {
        width: '100%',
        backgroundColor: '#3686FF'
    }
  });

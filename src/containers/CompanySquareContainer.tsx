import React, {FC } from 'react';
import { StyleSheet, View, Text} from 'react-native'
import ElevatedSquare from '../components/ElevatedSquare';
import { NavigationProp, useNavigation } from '@react-navigation/native';
import { CategoriesStackParamList } from '../../navigation/CategoriesStack';
import { Image } from 'react-native-elements';
import LocationPointerIcon from '../../icons/LocationPointerIcon';

interface CompanySquareContainerProps {
}

const CompanySquareContainer: FC<CompanySquareContainerProps>  = (props: CompanySquareContainerProps) => {
    const navigation = useNavigation<NavigationProp<CategoriesStackParamList, 'Companies'>>();

    const onPressHandler = () => {
    }

    return (
        <View>
            <ElevatedSquare onPress={onPressHandler} containerStyle={styles.elevatedSquareContainer} squareStyle={styles.elevatedSquare}>
                <View style={styles.squareContainer}>
                    <View>
                        <Image source={require('../../assets/png/WirecastGoCompanyLogo.png')} containerStyle={styles.imageContainer} style={styles.image}/>
                    </View>
                    <View style={styles.textContainer}>
                        <Text style={styles.companyNameText}>Planty</Text>
                        <View style={styles.locationContainer}>
                            <LocationPointerIcon width={styles.icon.width} height={styles.icon.height}/>
                            <Text style={styles.locationText}>Zagreb</Text>
                        </View>
                    </View>
                </View>
            </ElevatedSquare>
        </View>
    )
}

const styles = StyleSheet.create({
    image: {
        margin: 10,
        width: 60,
        height: 60,
    },
    imageContainer: {
        elevation: 5,
        borderRadius: 20,
        width: 80, 
        height: 80, 
        backgroundColor: 'lightblue',
        justifyContent: 'center',
        alignItems: 'center'
    },
    icon: {
        width: 15,
        height: 15,
    },
    companyNameText: {
        fontSize: 20,
        fontWeight: '600'
    },
    locationText: {
        marginLeft: '7%',
        fontWeight: '400',
        fontSize: 13
    },
    locationContainer: {
        alignItems: 'center',
        marginTop: '3%',
        flexDirection: 'row'
    },
    squareContainer: {
        flexDirection: 'row'
    },
    textContainer: {
        marginLeft: '3%',
    },
    elevatedSquareContainer: {
        height: 130,
    },
    elevatedSquare: {
        padding: '5%'
    },
  });

export default CompanySquareContainer;